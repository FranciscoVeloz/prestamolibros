﻿drop procedure if exists p_ActPrestamoAlumno;

create procedure p_ActPrestamoAlumno(
	in p_idPrestamo,
	in p_fkAlumno int,
	in p_fkLibro int,
	in p_fechaPrestamo date
)

p_ActPrestamoAlumno : begin
	
	declare devolucion date default 0;

	select DATE_ADD(p_fechaPrestamo, interval 3 DAY) into devolucion;

	update prestamoalumnos set fk_alumno = p_fkAlumno, fk_libro = p_fkLibro, fecha_prestamo = p_fechaPrestamo, fecha_devolucion = devolucion
	where id_prestamo = p_idPrestamo

end;

call p_ActPrestamoAlumno(1, 1, 3, '2020-10-22');
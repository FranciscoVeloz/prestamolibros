﻿--Creamos la base de datos
create database PrestamoLibros;
use PrestamoLibros;

--Creamos la tabla Alumnos
create table Alumnos(
	id_alumno int primary key auto_increment,
	nombre_alumno varchar(100),
	grado int,
	grupo varchar(100)
);

--Creamos la tabla Maestros
create table Maestros(
	id_maestro int primary key auto_increment,
	nombre_maestro varchar(100)
);

--Creamos la tabla Libros
create table Libros(
	id_libro int primary key auto_increment,
	nombre_libro varchar(100),
	editorial varchar(100)
);

--Creamos la tabla PrestamoAlumnos
create table PrestamoAlumnos(
	id_prestamo int primary key auto_increment,
	fk_alumno int,
	fk_libro int,
	fecha_prestamo date,
	fecha_devolucion date,
	foreign key(fk_alumno) references Alumnos(id_alumno),
	foreign key(fk_libro) references Libros(id_libro)
)

--Creamos la tabla PrestamoMaestros
create table PrestamoMaestros(
	id_prestamo int primary key auto_increment,
	fk_maestro int,
	fk_libro int,
	fecha_prestamo date,
	fecha_devolucion date,
	foreign key(fk_maestro) references Maestros(id_maestro),
	foreign key(fk_libro) references Libros(id_libro)
)

--Creacion de la tabla Administradores
create table Administradores(
	id_admin int primary key auto_increment,
	nombre_admin varchar(100),
	password varchar(100)
);

--Drop por si actualizamos las tablas
drop table Alumnos;
drop table Maestros;
drop table Libros;
drop table PrestamosAlumnos;
drop table PrestamosMaestros;

--Insert en las tablas
insert into Alumnos values
(null, 'Francisco Gonzalez Veloz', 5, 'A'),
(null, 'Daneli Reyes Gonzalez', 5, 'A'),
(null, 'Gabriel Hernandez Belmonte', 5, 'B');

insert into Maestros values
(null, 'John Freddy Vega'),
(null, 'Nestor Chico Rojas'),
(null, 'David Segundo Martin');

insert into Libros values
(null, 'Hunter x Hunter', 'Shonen jump'),
(null, 'FMAB', 'Shonen jump'),
(null, 'Naruto', 'Shonen jump');

insert into PrestamoAlumnos values
(null, 1, 1, '2020-10-22', '2020-10-25'),
(null, 2, 2, '2020-10-22', '2020-10-25'),
(null, 3, 3, '2020-10-22', '2020-10-25');

insert into PrestamoMaestros values
(null, 1, 1, '2020-10-22', '2020-10-25'),
(null, 2, 2, '2020-10-22', '2020-10-25'),
(null, 3, 3, '2020-10-22', '2020-10-25');

insert into Administradores values
(null, 'Francisco', 'contraseña');

--Creacion de las vistas para PrestamosAlumnos
drop view v_prestamosAlumnos

create view v_prestamosAlumnos as 
select id_prestamo, fk_alumno, nombre_alumno, fk_libro, nombre_libro, fecha_prestamo, fecha_devolucion 
from PrestamoAlumnos, Alumnos, Libros where id_alumno = fk_alumno && id_libro = fk_libro;

--Creacion de las vistas para PrestamosMaestros
drop view v_prestamosMaestros

create view v_prestamosMaestros as 
select id_prestamo, fk_maestro, nombre_maestro, fk_libro, nombre_libro, fecha_prestamo, fecha_devolucion 
from PrestamoMaestros, Maestros, Libros where id_maestro = fk_maestro && id_libro = fk_libro;
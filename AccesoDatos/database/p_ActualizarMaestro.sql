﻿drop procedure if exists p_ActPrestamoMaestro;

create procedure p_ActPrestamoMaestro(
	in p_idPrestamo int,
	in p_fkMaestro int,
	in p_fkLibro int,
	in p_fechaPrestamo date
)

p_ActPrestamoMaestro : begin

	declare devolucion date default 0;

	select DATE_ADD(p_fechaPrestamo, interval 3 DAY) into devolucion;

	update PrestamoMaestros set fk_maestro = p_fkMaestro, fk_libro = p_fkLibro, fecha_prestamo = p_fechaPrestamo, fecha_devolucion = devolucion
	where id_prestamo = p_idPrestamo;

end;

call p_PrestamoMaestro(1, 2, 2, '2020-10-22');
﻿drop procedure if exists p_PrestamoAlumno;

create procedure p_PrestamoAlumno(
	in p_fkAlumno int,
	in p_fkLibro int,
	in p_fechaPrestamo date
)

p_PrestamoAlumno : begin
	
	declare devolucion date default 0;

	select DATE_ADD(p_fechaPrestamo, interval 3 DAY) into devolucion;

	insert into PrestamoAlumnos values(null, p_fkAlumno, p_fkLibro, p_fechaPrestamo, devolucion);

end;

call p_PrestamoAlumno(1, 1, '2020-10-22');
﻿drop procedure if exists p_PrestamoMaestro;

create procedure p_PrestamoMaestro(
	in p_fkMaestro int,
	in p_fkLibro int,
	in p_fechaPrestamo date
)

p_PrestamoMaestro : begin

	declare devolucion date default 0;

	select DATE_ADD(p_fechaPrestamo, interval 3 DAY) into devolucion;

	insert into PrestamoMaestros values(null, p_fkMaestro, p_fkLibro, p_fechaPrestamo, devolucion);

end;

call p_PrestamoMaestro(2, 2, '2020-10-22');
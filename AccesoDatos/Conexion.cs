﻿using System.Data;
using Bases;

namespace AccesoDatos
{
    public class Conexion
    {
        Conectar conectar = new Conectar("localhost", "root", "", "prestamolibros");

        public string Comando(string query)
        {
            return conectar.Comando(query);
        }

        public DataSet Mostrar(string query, string tabla)
        {
            return conectar.Consultar(query, tabla);
        }
    }
}

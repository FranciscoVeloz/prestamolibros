﻿namespace Entidades
{
    public class PrestamoAlumnos
    {
        public int IdPrestamoAlumno { get; set; }
        public int FkAlumno { get; set; }
        public int FkLibro { get; set; }
        public string FechaPrestamo { get; set; }
        public string FechaDevolucion { get; set; }

        public PrestamoAlumnos(int idPrestamoAlumno, int fkAlumno, int fkLibro, string fechaPrestamo, string fechaDevolucion)
        {
            IdPrestamoAlumno = idPrestamoAlumno;
            FkAlumno = fkAlumno;
            FkLibro = fkLibro;
            FechaPrestamo = fechaPrestamo;
            FechaDevolucion = fechaDevolucion;
        }
    }
}

﻿namespace Entidades
{
    public class Alumnos
    {
        public int IdAlumno { get; set; }
        public string NombreAlumno { get; set; }
        public int Grado { get; set; }
        public string Grupo { get; set; }

        public Alumnos(int idAlumno, string nombreAlumno, int grado, string grupo)
        {
            IdAlumno = idAlumno;
            NombreAlumno = nombreAlumno;
            Grado = grado;
            Grupo = grupo;
        }
    }
}

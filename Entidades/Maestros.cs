﻿namespace Entidades
{
    public class Maestros
    {
        public int IdMaestro { get; set; }
        public string NombreMaestro { get; set; }

        public Maestros(int idMaestro, string nombreMaestro)
        {
            IdMaestro = idMaestro;
            NombreMaestro = nombreMaestro;
        }
    }
}
﻿namespace Entidades
{
    public class Administradores
    {
        public int IdAdmin { get; set; }
        public string Nombre { get; set; }
        public string Password { get; set; }

        public Administradores(int idAdmin, string nombre, string password)
        {
            IdAdmin = idAdmin;
            Nombre = nombre;
            Password = password;
        }
    }
}

﻿namespace Entidades
{
    public class Libros
    {
        public int IdLibro { get; set; }
        public string NombreLibro { get; set; }
        public string Editorial { get; set; }

        public Libros(int idLibro, string nombreLibro, string editorial)
        {
            IdLibro = idLibro;
            NombreLibro = nombreLibro;
            Editorial = editorial;
        }
    }
}

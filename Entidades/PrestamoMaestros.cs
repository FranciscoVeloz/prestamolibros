﻿namespace Entidades
{
    public class PrestamoMaestros
    {
        public int IdPrestamoMaestro { get; set; }
        public int FkMaestro { get; set; }
        public int FkLibro { get; set; }
        public string FechaPrestamo { get; set; }
        public string FechaDevolucion { get; set; }

        public PrestamoMaestros(int idPrestamoMaestro, int fkMaestro, int fkLibro, string fechaPrestamo, string fechaDevolucion)
        {
            IdPrestamoMaestro = idPrestamoMaestro;
            FkMaestro = fkMaestro;
            FkLibro = fkLibro;
            FechaPrestamo = fechaPrestamo;
            FechaDevolucion = fechaDevolucion;
        }
    }
}

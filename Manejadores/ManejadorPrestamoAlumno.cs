﻿using System.Data;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorPrestamoAlumno
    {
        Conexion conexion = new Conexion();

        ////Insertar datos
        public string Insertar(PrestamoAlumnos prestamoalumnos)
        {
            return conexion.Comando($"call p_PrestamoAlumno({prestamoalumnos.FkAlumno}, {prestamoalumnos.FkLibro}," +
                $" '{prestamoalumnos.FechaPrestamo}')");
        }

        ////Eliminar datos
        public string Eliminar(PrestamoAlumnos prestamoalumnos)
        {
            return conexion.Comando($"delete from prestamoalumnos where id_prestamo = {prestamoalumnos.IdPrestamoAlumno}");
        }

        ////Actualizar datos
        public string Actualizar(PrestamoAlumnos prestamoalumnos)
        {
            return conexion.Comando($"call p_ActPrestamoAlumno({prestamoalumnos.IdPrestamoAlumno}, {prestamoalumnos.FkAlumno}," +
                $" {prestamoalumnos.FkLibro}, '{prestamoalumnos.FechaPrestamo}')");
        }

        ////Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}

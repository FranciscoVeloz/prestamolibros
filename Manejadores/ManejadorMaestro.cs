﻿using System.Data;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorMaestro
    {
        Conexion conexion = new Conexion();

        ////Insertar datos
        public string Insertar(Maestros maestros)
        {
            return conexion.Comando($"insert into maestros values(null, '{maestros.NombreMaestro}')");
        }

        ////Eliminar datos
        public string Eliminar(Maestros maestros)
        {
            return conexion.Comando($"delete from maestros where id_maestro = {maestros.IdMaestro}");
        }

        ////Actualizar datos
        public string Actualizar(Maestros maestros)
        {
            return conexion.Comando($"update maestros set nombre_maestro = '{maestros.NombreMaestro}' where id_maestro = {maestros.IdMaestro}");
        }

        ////Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}

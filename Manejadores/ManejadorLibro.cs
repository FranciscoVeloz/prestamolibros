﻿using System.Data;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorLibro
    {
        Conexion conexion = new Conexion();

        ////Insertar datos
        public string Insertar(Libros libros)
        {
            return conexion.Comando($"insert into libros values(null, '{libros.NombreLibro}', '{libros.Editorial}')");
        }

        ////Eliminar datos
        public string Eliminar(Libros libros)
        {
            return conexion.Comando($"delete from libros where id_libro = {libros.IdLibro}");
        }

        ////Actualizar datos
        public string Actualizar(Libros libros)
        {
            return conexion.Comando($"update libros set nombre_libro = '{libros.NombreLibro}', " +
                $"editorial = '{libros.Editorial}' where id_libro = {libros.IdLibro}");
        }

        ////Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}

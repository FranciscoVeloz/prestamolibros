﻿using System.Data;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorAlumno
    {
        Conexion conexion = new Conexion();

        ////Insertar datos
        public string Insertar(Alumnos alumnos)
        {
            return conexion.Comando($"insert into alumnos values(null, '{alumnos.NombreAlumno}', {alumnos.Grado}, '{alumnos.Grupo}')");
        }

        ////Eliminar datos
        public string Eliminar(Alumnos alumnos)
        {
            return conexion.Comando($"delete from alumnos where id_alumno = {alumnos.IdAlumno}");
        }

        ////Actualizar datos
        public string Actualizar(Alumnos alumnos)
        {
            return conexion.Comando($"update alumnos set nombre_alumno = '{alumnos.NombreAlumno}', grado = {alumnos.Grado}, " +
                $"grupo = '{alumnos.Grupo}' where id_alumno = {alumnos.IdAlumno}");
        }

        ////Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}

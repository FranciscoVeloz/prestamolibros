﻿using System.Data;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorPrestamoMaestro
    {
        Conexion conexion = new Conexion();

        ////Insertar datos
        public string Insertar(PrestamoMaestros prestamomaestros)
        {
            return conexion.Comando($"call p_PrestamoMaestro({prestamomaestros.FkMaestro}, {prestamomaestros.FkLibro}," +
                $" '{prestamomaestros.FechaPrestamo}')");
        }

        ////Eliminar datos
        public string Eliminar(PrestamoMaestros prestamomaestros)
        {
            return conexion.Comando($"delete from prestamomaestros where id_prestamo = {prestamomaestros.IdPrestamoMaestro}");
        }

        ////Actualizar datos
        public string Actualizar(PrestamoMaestros prestamomaestros)
        {
            return conexion.Comando($"call p_ActPrestamoMaestro({prestamomaestros.IdPrestamoMaestro}, {prestamomaestros.FkMaestro}, " +
                $"{prestamomaestros.FkLibro}, '{prestamomaestros.FechaPrestamo}')");
        }

        ////Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}

﻿using System.Data;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorAdmin
    {
        Conexion conexion = new Conexion();

        ////Insertar datos
        public string Insertar(Administradores administradores)
        {
            return conexion.Comando($"insert into administradores values(null, '{administradores.Nombre}', '{administradores.Password}')");
        }

        ////Eliminar datos
        public string Eliminar(Administradores administradores)
        {
            return conexion.Comando($"delete from administradores where id_admin = {administradores.IdAdmin}");
        }

        ////Actualizar datos
        public string Actualizar(Administradores administradores)
        {
            return conexion.Comando($"update administradores set nombre_admin = '{administradores.Nombre}', " +
                $"password = '{administradores.Password}' where id_admin = {administradores.IdAdmin}");
        }

        ////Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}

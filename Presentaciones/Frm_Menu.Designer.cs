﻿namespace Presentacion
{
    partial class Frm_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Menu));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.OptAlumnos = new System.Windows.Forms.ToolStripButton();
            this.OptMaestros = new System.Windows.Forms.ToolStripButton();
            this.OptLibros = new System.Windows.Forms.ToolStripButton();
            this.OptPrestamos = new System.Windows.Forms.ToolStripButton();
            this.OptRegistrar = new System.Windows.Forms.ToolStripButton();
            this.OptSalir = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(240)))));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptAlumnos,
            this.OptMaestros,
            this.OptLibros,
            this.OptPrestamos,
            this.OptRegistrar,
            this.OptSalir});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(69, 495);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // OptAlumnos
            // 
            this.OptAlumnos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptAlumnos.Image = ((System.Drawing.Image)(resources.GetObject("OptAlumnos.Image")));
            this.OptAlumnos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptAlumnos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptAlumnos.Name = "OptAlumnos";
            this.OptAlumnos.Size = new System.Drawing.Size(66, 68);
            this.OptAlumnos.Text = "Alumnos";
            this.OptAlumnos.Click += new System.EventHandler(this.OptAlumnos_Click);
            // 
            // OptMaestros
            // 
            this.OptMaestros.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptMaestros.Image = ((System.Drawing.Image)(resources.GetObject("OptMaestros.Image")));
            this.OptMaestros.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptMaestros.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptMaestros.Name = "OptMaestros";
            this.OptMaestros.Size = new System.Drawing.Size(66, 68);
            this.OptMaestros.Text = "Maestros";
            this.OptMaestros.Click += new System.EventHandler(this.OptMaestros_Click);
            // 
            // OptLibros
            // 
            this.OptLibros.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptLibros.Image = ((System.Drawing.Image)(resources.GetObject("OptLibros.Image")));
            this.OptLibros.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptLibros.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptLibros.Name = "OptLibros";
            this.OptLibros.Size = new System.Drawing.Size(66, 68);
            this.OptLibros.Text = "Libros";
            this.OptLibros.Click += new System.EventHandler(this.OptLibros_Click);
            // 
            // OptPrestamos
            // 
            this.OptPrestamos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptPrestamos.Image = ((System.Drawing.Image)(resources.GetObject("OptPrestamos.Image")));
            this.OptPrestamos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptPrestamos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptPrestamos.Name = "OptPrestamos";
            this.OptPrestamos.Size = new System.Drawing.Size(66, 68);
            this.OptPrestamos.Text = "Prestamos";
            this.OptPrestamos.Click += new System.EventHandler(this.OptPrestamos_Click);
            // 
            // OptRegistrar
            // 
            this.OptRegistrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptRegistrar.Image = ((System.Drawing.Image)(resources.GetObject("OptRegistrar.Image")));
            this.OptRegistrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptRegistrar.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.OptRegistrar.Name = "OptRegistrar";
            this.OptRegistrar.Size = new System.Drawing.Size(66, 68);
            this.OptRegistrar.Text = "Registrar";
            this.OptRegistrar.Click += new System.EventHandler(this.OptRegistrar_Click);
            // 
            // OptSalir
            // 
            this.OptSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptSalir.Image = ((System.Drawing.Image)(resources.GetObject("OptSalir.Image")));
            this.OptSalir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptSalir.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.OptSalir.Name = "OptSalir";
            this.OptSalir.Size = new System.Drawing.Size(66, 68);
            this.OptSalir.Text = "Salir";
            this.OptSalir.Click += new System.EventHandler(this.OptSalir_Click);
            // 
            // Frm_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(985, 495);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.Name = "Frm_Menu";
            this.ShowIcon = false;
            this.Text = "Libreria";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Menu_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton OptAlumnos;
        private System.Windows.Forms.ToolStripButton OptMaestros;
        private System.Windows.Forms.ToolStripButton OptLibros;
        private System.Windows.Forms.ToolStripButton OptPrestamos;
        private System.Windows.Forms.ToolStripButton OptSalir;
        private System.Windows.Forms.ToolStripButton OptRegistrar;
    }
}
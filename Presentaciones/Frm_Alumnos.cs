﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;


namespace Presentacion
{
    public partial class Frm_Alumnos : Form
    {
        Alumnos alumnos = new Alumnos(0, "", 0, "");
        ManejadorAlumno manejador = new ManejadorAlumno();

        int id = 0;
        public Frm_Alumnos()
        {
            InitializeComponent();
        }

        void ActualizarDtg()
        {
            DtgAlumnos.DataSource = manejador.Mostrar($"select * from alumnos", "alumnos").Tables[0];
            DtgAlumnos.AutoResizeColumns();
            LimpiarTxt();
            TxtNombre.Focus();
        }

        void LimpiarTxt()
        {
            TxtNombre.Clear();
            TxtGrado.Clear();
            TxtGrupo.Clear();
        }

        private void BtnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Frm_Alumnos_Load(object sender, EventArgs e)
        {
            ActualizarDtg();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            string resultado = manejador.Insertar(new Alumnos(0, TxtNombre.Text, int.Parse(TxtGrado.Text), TxtGrupo.Text));
            ActualizarDtg();
        }

        private void DtgAlumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            alumnos.IdAlumno = int.Parse(DtgAlumnos.Rows[e.RowIndex].Cells[0].Value.ToString());
            alumnos.NombreAlumno = DtgAlumnos.Rows[e.RowIndex].Cells[1].Value.ToString();
            alumnos.Grado = int.Parse(DtgAlumnos.Rows[e.RowIndex].Cells[2].Value.ToString());
            alumnos.Grupo = DtgAlumnos.Rows[e.RowIndex].Cells[3].Value.ToString();

            id = alumnos.IdAlumno;
            TxtNombre.Text = alumnos.NombreAlumno;
            TxtGrado.Text = alumnos.Grado.ToString();
            TxtGrupo.Text = alumnos.Grupo;
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejador.Actualizar(alumnos = new Alumnos(id, TxtNombre.Text, int.Parse(TxtGrado.Text), TxtGrupo.Text));
            ActualizarDtg();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar al alumno: {alumnos.NombreAlumno}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                manejador.Eliminar(alumnos);
                ActualizarDtg();
            }
        }
    }
}

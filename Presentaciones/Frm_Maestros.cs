﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Maestros : Form
    {
        Maestros maestros = new Maestros(0, "");
        ManejadorMaestro manejador = new ManejadorMaestro();

        int id = 0;

        public Frm_Maestros()
        {
            InitializeComponent();
        }

        void ActualizarDtg()
        {
            DtgMaestros.DataSource = manejador.Mostrar($"select * from maestros", "maestros").Tables[0];
            DtgMaestros.AutoResizeColumns();
            LimpiarTxt();
            TxtNombre.Focus();
        }

        void LimpiarTxt()
        {
            TxtNombre.Clear();
        }

        private void BtnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Frm_Maestros_Load(object sender, EventArgs e)
        {
            ActualizarDtg();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            string resultado = manejador.Insertar(new Maestros(0, TxtNombre.Text));
            ActualizarDtg();
        }

        private void DtgMaestros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            maestros.IdMaestro = int.Parse(DtgMaestros.Rows[e.RowIndex].Cells[0].Value.ToString());
            maestros.NombreMaestro = DtgMaestros.Rows[e.RowIndex].Cells[1].Value.ToString();

            id = maestros.IdMaestro;
            TxtNombre.Text = maestros.NombreMaestro;
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejador.Actualizar(maestros = new Maestros(id, TxtNombre.Text));
            ActualizarDtg();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar al maestro: {maestros.NombreMaestro}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                manejador.Eliminar(maestros);
                ActualizarDtg();
            }
        }
    }
}

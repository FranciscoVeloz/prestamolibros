﻿namespace Presentacion
{
    partial class Frm_Prestamos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Prestamos));
            this.BtnRegresar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Alumnos = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnActualizarAlumno = new System.Windows.Forms.Button();
            this.BtnAgregarAlumno = new System.Windows.Forms.Button();
            this.BtnEliminarAlumno = new System.Windows.Forms.Button();
            this.DtgAlumnos = new System.Windows.Forms.DataGridView();
            this.TxtFechaAlumno = new System.Windows.Forms.TextBox();
            this.TxtLibroAlumno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtAlumno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnActualizarMaestro = new System.Windows.Forms.Button();
            this.BtnAgregarMaestro = new System.Windows.Forms.Button();
            this.BtnEliminarMaestro = new System.Windows.Forms.Button();
            this.DtgMaestro = new System.Windows.Forms.DataGridView();
            this.TxtFechaMaestro = new System.Windows.Forms.TextBox();
            this.TxtLibroMaestro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtMaestro = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Alumnos.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgAlumnos)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgMaestro)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnRegresar
            // 
            this.BtnRegresar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(203)))), ((int)(((byte)(242)))));
            this.BtnRegresar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(203)))), ((int)(((byte)(242)))));
            this.BtnRegresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnRegresar.Image = ((System.Drawing.Image)(resources.GetObject("BtnRegresar.Image")));
            this.BtnRegresar.Location = new System.Drawing.Point(0, 0);
            this.BtnRegresar.Name = "BtnRegresar";
            this.BtnRegresar.Size = new System.Drawing.Size(50, 50);
            this.BtnRegresar.TabIndex = 8;
            this.BtnRegresar.UseVisualStyleBackColor = false;
            this.BtnRegresar.Click += new System.EventHandler(this.BtnRegresar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(203)))), ((int)(((byte)(242)))));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 50);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(203)))), ((int)(((byte)(242)))));
            this.label1.Font = new System.Drawing.Font("Arial", 20F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.label1.Location = new System.Drawing.Point(328, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Prestamos";
            // 
            // Alumnos
            // 
            this.Alumnos.Controls.Add(this.tabPage1);
            this.Alumnos.Controls.Add(this.tabPage2);
            this.Alumnos.Location = new System.Drawing.Point(0, 50);
            this.Alumnos.Name = "Alumnos";
            this.Alumnos.SelectedIndex = 0;
            this.Alumnos.Size = new System.Drawing.Size(800, 550);
            this.Alumnos.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(237)))), ((int)(((byte)(228)))));
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.BtnActualizarAlumno);
            this.tabPage1.Controls.Add(this.BtnAgregarAlumno);
            this.tabPage1.Controls.Add(this.BtnEliminarAlumno);
            this.tabPage1.Controls.Add(this.DtgAlumnos);
            this.tabPage1.Controls.Add(this.TxtFechaAlumno);
            this.tabPage1.Controls.Add(this.TxtLibroAlumno);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.TxtAlumno);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 519);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Alumnos";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 17F);
            this.label4.Location = new System.Drawing.Point(25, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(177, 26);
            this.label4.TabIndex = 14;
            this.label4.Text = "Fecha prestamo";
            // 
            // BtnActualizarAlumno
            // 
            this.BtnActualizarAlumno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(241)))), ((int)(((byte)(242)))));
            this.BtnActualizarAlumno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnActualizarAlumno.Font = new System.Drawing.Font("Arial", 15F);
            this.BtnActualizarAlumno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.BtnActualizarAlumno.Location = new System.Drawing.Point(575, 150);
            this.BtnActualizarAlumno.Name = "BtnActualizarAlumno";
            this.BtnActualizarAlumno.Size = new System.Drawing.Size(210, 60);
            this.BtnActualizarAlumno.TabIndex = 6;
            this.BtnActualizarAlumno.Text = "Actualizar";
            this.BtnActualizarAlumno.UseVisualStyleBackColor = false;
            this.BtnActualizarAlumno.Click += new System.EventHandler(this.BtnActualizarAlumno_Click);
            // 
            // BtnAgregarAlumno
            // 
            this.BtnAgregarAlumno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(214)))), ((int)(((byte)(73)))));
            this.BtnAgregarAlumno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAgregarAlumno.Font = new System.Drawing.Font("Arial", 15F);
            this.BtnAgregarAlumno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(240)))));
            this.BtnAgregarAlumno.Location = new System.Drawing.Point(575, 84);
            this.BtnAgregarAlumno.Name = "BtnAgregarAlumno";
            this.BtnAgregarAlumno.Size = new System.Drawing.Size(100, 60);
            this.BtnAgregarAlumno.TabIndex = 4;
            this.BtnAgregarAlumno.Text = "Agregar";
            this.BtnAgregarAlumno.UseVisualStyleBackColor = false;
            this.BtnAgregarAlumno.Click += new System.EventHandler(this.BtnAgregarAlumno_Click);
            // 
            // BtnEliminarAlumno
            // 
            this.BtnEliminarAlumno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.BtnEliminarAlumno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEliminarAlumno.Font = new System.Drawing.Font("Arial", 15F);
            this.BtnEliminarAlumno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(240)))));
            this.BtnEliminarAlumno.Location = new System.Drawing.Point(685, 84);
            this.BtnEliminarAlumno.Name = "BtnEliminarAlumno";
            this.BtnEliminarAlumno.Size = new System.Drawing.Size(100, 60);
            this.BtnEliminarAlumno.TabIndex = 5;
            this.BtnEliminarAlumno.Text = "Eliminar";
            this.BtnEliminarAlumno.UseVisualStyleBackColor = false;
            this.BtnEliminarAlumno.Click += new System.EventHandler(this.BtnEliminarAlumno_Click);
            // 
            // DtgAlumnos
            // 
            this.DtgAlumnos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(240)))));
            this.DtgAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgAlumnos.Location = new System.Drawing.Point(8, 290);
            this.DtgAlumnos.Name = "DtgAlumnos";
            this.DtgAlumnos.ReadOnly = true;
            this.DtgAlumnos.RowTemplate.Height = 24;
            this.DtgAlumnos.Size = new System.Drawing.Size(776, 221);
            this.DtgAlumnos.TabIndex = 7;
            this.DtgAlumnos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgAlumnos_CellContentClick);
            // 
            // TxtFechaAlumno
            // 
            this.TxtFechaAlumno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtFechaAlumno.Location = new System.Drawing.Point(25, 215);
            this.TxtFechaAlumno.Name = "TxtFechaAlumno";
            this.TxtFechaAlumno.Size = new System.Drawing.Size(498, 26);
            this.TxtFechaAlumno.TabIndex = 3;
            this.TxtFechaAlumno.Text = "YYYY-MM-DD";
            // 
            // TxtLibroAlumno
            // 
            this.TxtLibroAlumno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtLibroAlumno.Location = new System.Drawing.Point(25, 130);
            this.TxtLibroAlumno.Name = "TxtLibroAlumno";
            this.TxtLibroAlumno.Size = new System.Drawing.Size(498, 26);
            this.TxtLibroAlumno.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 17F);
            this.label3.Location = new System.Drawing.Point(25, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 26);
            this.label3.TabIndex = 9;
            this.label3.Text = "Id Libro";
            // 
            // TxtAlumno
            // 
            this.TxtAlumno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtAlumno.Location = new System.Drawing.Point(25, 50);
            this.TxtAlumno.Name = "TxtAlumno";
            this.TxtAlumno.Size = new System.Drawing.Size(498, 26);
            this.TxtAlumno.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 17F);
            this.label2.Location = new System.Drawing.Point(25, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 26);
            this.label2.TabIndex = 9;
            this.label2.Text = "Id Alumno";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(237)))), ((int)(((byte)(228)))));
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.BtnActualizarMaestro);
            this.tabPage2.Controls.Add(this.BtnAgregarMaestro);
            this.tabPage2.Controls.Add(this.BtnEliminarMaestro);
            this.tabPage2.Controls.Add(this.DtgMaestro);
            this.tabPage2.Controls.Add(this.TxtFechaMaestro);
            this.tabPage2.Controls.Add(this.TxtLibroMaestro);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.TxtMaestro);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 519);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Maestros";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 17F);
            this.label5.Location = new System.Drawing.Point(25, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(177, 26);
            this.label5.TabIndex = 24;
            this.label5.Text = "Fecha prestamo";
            // 
            // BtnActualizarMaestro
            // 
            this.BtnActualizarMaestro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(241)))), ((int)(((byte)(242)))));
            this.BtnActualizarMaestro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnActualizarMaestro.Font = new System.Drawing.Font("Arial", 15F);
            this.BtnActualizarMaestro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.BtnActualizarMaestro.Location = new System.Drawing.Point(575, 150);
            this.BtnActualizarMaestro.Name = "BtnActualizarMaestro";
            this.BtnActualizarMaestro.Size = new System.Drawing.Size(210, 60);
            this.BtnActualizarMaestro.TabIndex = 6;
            this.BtnActualizarMaestro.Text = "Actualizar";
            this.BtnActualizarMaestro.UseVisualStyleBackColor = false;
            this.BtnActualizarMaestro.Click += new System.EventHandler(this.BtnActualizarMaestro_Click);
            // 
            // BtnAgregarMaestro
            // 
            this.BtnAgregarMaestro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(214)))), ((int)(((byte)(73)))));
            this.BtnAgregarMaestro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAgregarMaestro.Font = new System.Drawing.Font("Arial", 15F);
            this.BtnAgregarMaestro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(240)))));
            this.BtnAgregarMaestro.Location = new System.Drawing.Point(575, 84);
            this.BtnAgregarMaestro.Name = "BtnAgregarMaestro";
            this.BtnAgregarMaestro.Size = new System.Drawing.Size(100, 60);
            this.BtnAgregarMaestro.TabIndex = 4;
            this.BtnAgregarMaestro.Text = "Agregar";
            this.BtnAgregarMaestro.UseVisualStyleBackColor = false;
            this.BtnAgregarMaestro.Click += new System.EventHandler(this.BtnAgregarMaestro_Click);
            // 
            // BtnEliminarMaestro
            // 
            this.BtnEliminarMaestro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.BtnEliminarMaestro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEliminarMaestro.Font = new System.Drawing.Font("Arial", 15F);
            this.BtnEliminarMaestro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(240)))));
            this.BtnEliminarMaestro.Location = new System.Drawing.Point(684, 84);
            this.BtnEliminarMaestro.Name = "BtnEliminarMaestro";
            this.BtnEliminarMaestro.Size = new System.Drawing.Size(100, 60);
            this.BtnEliminarMaestro.TabIndex = 5;
            this.BtnEliminarMaestro.Text = "Eliminar";
            this.BtnEliminarMaestro.UseVisualStyleBackColor = false;
            this.BtnEliminarMaestro.Click += new System.EventHandler(this.BtnEliminarMaestro_Click);
            // 
            // DtgMaestro
            // 
            this.DtgMaestro.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(240)))));
            this.DtgMaestro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgMaestro.Location = new System.Drawing.Point(8, 290);
            this.DtgMaestro.Name = "DtgMaestro";
            this.DtgMaestro.ReadOnly = true;
            this.DtgMaestro.RowTemplate.Height = 24;
            this.DtgMaestro.Size = new System.Drawing.Size(776, 221);
            this.DtgMaestro.TabIndex = 7;
            this.DtgMaestro.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgMaestro_CellContentClick);
            // 
            // TxtFechaMaestro
            // 
            this.TxtFechaMaestro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtFechaMaestro.Location = new System.Drawing.Point(25, 215);
            this.TxtFechaMaestro.Name = "TxtFechaMaestro";
            this.TxtFechaMaestro.Size = new System.Drawing.Size(498, 26);
            this.TxtFechaMaestro.TabIndex = 3;
            this.TxtFechaMaestro.Text = "YYYY-MM-DD";
            // 
            // TxtLibroMaestro
            // 
            this.TxtLibroMaestro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtLibroMaestro.Location = new System.Drawing.Point(25, 130);
            this.TxtLibroMaestro.Name = "TxtLibroMaestro";
            this.TxtLibroMaestro.Size = new System.Drawing.Size(498, 26);
            this.TxtLibroMaestro.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 17F);
            this.label6.Location = new System.Drawing.Point(25, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 26);
            this.label6.TabIndex = 18;
            this.label6.Text = "Id Libro";
            // 
            // TxtMaestro
            // 
            this.TxtMaestro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtMaestro.Location = new System.Drawing.Point(25, 50);
            this.TxtMaestro.Name = "TxtMaestro";
            this.TxtMaestro.Size = new System.Drawing.Size(498, 26);
            this.TxtMaestro.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 17F);
            this.label7.Location = new System.Drawing.Point(25, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 26);
            this.label7.TabIndex = 19;
            this.label7.Text = "Id Maestro";
            // 
            // Frm_Prestamos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.Alumnos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnRegresar);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Prestamos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Prestamos";
            this.Load += new System.EventHandler(this.Frm_Prestamos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Alumnos.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgAlumnos)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgMaestro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnRegresar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl Alumnos;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button BtnActualizarAlumno;
        private System.Windows.Forms.Button BtnAgregarAlumno;
        private System.Windows.Forms.Button BtnEliminarAlumno;
        private System.Windows.Forms.DataGridView DtgAlumnos;
        private System.Windows.Forms.TextBox TxtAlumno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtLibroAlumno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtFechaAlumno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnActualizarMaestro;
        private System.Windows.Forms.Button BtnAgregarMaestro;
        private System.Windows.Forms.Button BtnEliminarMaestro;
        private System.Windows.Forms.DataGridView DtgMaestro;
        private System.Windows.Forms.TextBox TxtFechaMaestro;
        private System.Windows.Forms.TextBox TxtLibroMaestro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtMaestro;
        private System.Windows.Forms.Label label7;
    }
}
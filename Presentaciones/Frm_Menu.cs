﻿using System;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_Menu : Form
    {
        public Frm_Menu()
        {
            InitializeComponent();
        }

        private void OptSalir_Click(object sender, EventArgs e)
        {
            Frm_login login = new Frm_login();
            login.ShowDialog();
        }

        private void OptAlumnos_Click(object sender, EventArgs e)
        {
            Frm_Alumnos alumnos = new Frm_Alumnos();
            alumnos.ShowDialog();
        }

        private void OptMaestros_Click(object sender, EventArgs e)
        {
            Frm_Maestros maestros = new Frm_Maestros();
            maestros.ShowDialog();
        }

        private void OptLibros_Click(object sender, EventArgs e)
        {
            Frm_Libros libros = new Frm_Libros();
            libros.ShowDialog();
        }

        private void OptPrestamos_Click(object sender, EventArgs e)
        {
            Frm_Prestamos prestamos = new Frm_Prestamos();
            prestamos.ShowDialog();
        }

        private void OptRegistrar_Click(object sender, EventArgs e)
        {
            Frm_signup signup = new Frm_signup();
            signup.ShowDialog();
        }

        private void Frm_Menu_Load(object sender, EventArgs e)
        {
            Frm_login login = new Frm_login();
            login.ShowDialog();
        }
    }
}

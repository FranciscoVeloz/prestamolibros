﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_signup : Form
    {
        Administradores administradores = new Administradores(0, "", "");
        ManejadorAdmin manejador = new ManejadorAdmin();

        int id = 0;

        public Frm_signup()
        {
            InitializeComponent();
        }
        void ActualizarDtg()
        {
            DtgAdmin.DataSource = manejador.Mostrar($"select * from administradores", "administradores").Tables[0];
            DtgAdmin.AutoResizeColumns();
            LimpiarTxt();
            TxtNombre.Focus();
        }

        void LimpiarTxt()
        {
            TxtNombre.Clear();
            TxtPass.Clear();
        }

        private void Frm_signup_Load(object sender, EventArgs e)
        {
            ActualizarDtg();
        }
        private void BtnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            string resultado = manejador.Insertar(new Administradores(0, TxtNombre.Text, TxtPass.Text));
            ActualizarDtg();
        }

        private void DtgAdmin_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            administradores.IdAdmin = int.Parse(DtgAdmin.Rows[e.RowIndex].Cells[0].Value.ToString());
            administradores.Nombre = DtgAdmin.Rows[e.RowIndex].Cells[1].Value.ToString();
            administradores.Password = DtgAdmin.Rows[e.RowIndex].Cells[2].Value.ToString();

            id = administradores.IdAdmin;
            TxtNombre.Text = administradores.Nombre;
            TxtPass.Text = administradores.Password;
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejador.Actualizar(administradores = new Administradores(id, TxtNombre.Text, TxtPass.Text));
            ActualizarDtg();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar al admin: {administradores.Nombre}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                manejador.Eliminar(administradores);
                ActualizarDtg();
            }
        }
    }
}

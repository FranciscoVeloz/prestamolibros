﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Prestamos : Form
    {
        PrestamoAlumnos prestamoAlumnos = new PrestamoAlumnos(0, 0, 0, "", "");
        PrestamoMaestros prestamoMaestros = new PrestamoMaestros(0, 0, 0, "", "");
        ManejadorPrestamoAlumno manejadorAlumno = new ManejadorPrestamoAlumno();
        ManejadorPrestamoMaestro manejadorMaestro = new ManejadorPrestamoMaestro();

        int idAlumno = 0;
        int idMaestro = 0;
        public Frm_Prestamos()
        {
            InitializeComponent();
        }

        private void BtnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }
        void ActualizarDtg()
        {
            DtgAlumnos.DataSource = manejadorAlumno.Mostrar($"select * from v_prestamosAlumnos", "v_prestamosAlumnos").Tables[0];
            DtgMaestro.DataSource = manejadorMaestro.Mostrar($"select * from v_prestamosMaestros", "v_prestamosMaestros").Tables[0];
            DtgAlumnos.AutoResizeColumns();
            DtgMaestro.AutoResizeColumns();
            LimpiarTxt();
        }

        void LimpiarTxt()
        {
            TxtAlumno.Clear();
            TxtFechaAlumno.Text = "YYYY-MM-DD";
            TxtFechaMaestro.Text = "YYYY-MM-DD";
            TxtLibroAlumno.Clear();
            TxtLibroMaestro.Clear();
            TxtMaestro.Clear();
        }

        private void Frm_Prestamos_Load(object sender, EventArgs e)
        {
            ActualizarDtg();
        }

        /////////////////////////////////// CRUD PARA ALUMNOS ////////////////////////////////////
        private void BtnAgregarAlumno_Click(object sender, EventArgs e)
        {
            string resultado = manejadorAlumno.Insertar(new PrestamoAlumnos
                (0, int.Parse(TxtAlumno.Text), int.Parse(TxtLibroAlumno.Text), TxtFechaAlumno.Text, ""));
            ActualizarDtg();
        }

        private void DtgAlumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            prestamoAlumnos.IdPrestamoAlumno = int.Parse(DtgAlumnos.Rows[e.RowIndex].Cells[0].Value.ToString());
            prestamoAlumnos.FkAlumno = int.Parse(DtgAlumnos.Rows[e.RowIndex].Cells[1].Value.ToString());
            prestamoAlumnos.FkLibro = int.Parse(DtgAlumnos.Rows[e.RowIndex].Cells[3].Value.ToString());
            prestamoAlumnos.FechaPrestamo = DtgAlumnos.Rows[e.RowIndex].Cells[5].Value.ToString();

            idAlumno = prestamoAlumnos.IdPrestamoAlumno;
            TxtAlumno.Text = prestamoAlumnos.FkAlumno.ToString();
            TxtLibroAlumno.Text = prestamoAlumnos.FkLibro.ToString();
        }

        private void BtnActualizarAlumno_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejadorAlumno.Actualizar(prestamoAlumnos = new PrestamoAlumnos(idAlumno, int.Parse(TxtAlumno.Text), int.Parse(TxtLibroAlumno.Text), TxtFechaAlumno.Text, ""));
            ActualizarDtg();
        }

        private void BtnEliminarAlumno_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar el prestamo: {prestamoAlumnos.IdPrestamoAlumno}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                manejadorAlumno.Eliminar(prestamoAlumnos);
                ActualizarDtg();
            }
        }
        /////////////////////////////////// CRUD PARA MAESTROS ///////////////////////////////////
        private void BtnAgregarMaestro_Click(object sender, EventArgs e)
        {
            string resultado = manejadorMaestro.Insertar(new PrestamoMaestros
                (0, int.Parse(TxtMaestro.Text), int.Parse(TxtLibroMaestro.Text), TxtFechaMaestro.Text, ""));
            ActualizarDtg();
        }

        private void DtgMaestro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            prestamoMaestros.IdPrestamoMaestro = int.Parse(DtgMaestro.Rows[e.RowIndex].Cells[0].Value.ToString());
            prestamoMaestros.FkMaestro = int.Parse(DtgMaestro.Rows[e.RowIndex].Cells[1].Value.ToString());
            prestamoMaestros.FkLibro = int.Parse(DtgMaestro.Rows[e.RowIndex].Cells[3].Value.ToString());
            prestamoMaestros.FechaPrestamo = DtgMaestro.Rows[e.RowIndex].Cells[5].Value.ToString();

            idMaestro = prestamoMaestros.IdPrestamoMaestro;
            TxtMaestro.Text = prestamoMaestros.FkMaestro.ToString();
            TxtLibroMaestro.Text = prestamoMaestros.FkLibro.ToString();
        }

        private void BtnActualizarMaestro_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejadorMaestro.Actualizar(prestamoMaestros = new PrestamoMaestros(idMaestro, int.Parse(TxtMaestro.Text), int.Parse(TxtLibroMaestro.Text), TxtFechaMaestro.Text, ""));
            ActualizarDtg();
        }

        private void BtnEliminarMaestro_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar el prestamo: {prestamoMaestros.IdPrestamoMaestro}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                manejadorMaestro.Eliminar(prestamoMaestros);
                ActualizarDtg();
            }
        }
    }
}

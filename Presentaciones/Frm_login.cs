﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_login : Form
    {
        ManejadorAdmin manejador = new ManejadorAdmin();

        List<string> admin = new List<string>();
        List<string> pass = new List<string>();

        public Frm_login()
        {
            InitializeComponent();
        }
        private void Frm_login_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < manejador.Mostrar($"select * from administradores", "administradoras").Tables[0].Rows.Count; i++)
            {
                admin.Add(manejador.Mostrar($"select * from administradores", "administradoras").Tables[0].Rows[i]["nombre_admin"].ToString());
                pass.Add(manejador.Mostrar($"select * from administradores", "administradoras").Tables[0].Rows[i]["password"].ToString());
            }
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            bool resultado = true;
            for (int i = 0; i < admin.Count; i++)
            {
                if (TxtNombre.Text.Equals(admin[i]) && TxtPass.Text.Equals(pass[i]))
                {
                    resultado = true; i = admin.Count();
                }

                else
                {
                    resultado = false;
                }
            }

            if(resultado)
            {
                Close();
            }
            
            else
            {
                MessageBox.Show("Error: un dato esta equivocado");
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

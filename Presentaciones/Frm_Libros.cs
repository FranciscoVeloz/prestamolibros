﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_Libros : Form
    {
        Libros libros = new Libros(0, "", "");
        ManejadorLibro manejador = new ManejadorLibro();

        int id = 0;

        public Frm_Libros()
        {
            InitializeComponent();
        }

        void ActualizarDtg()
        {
            DtgLibros.DataSource = manejador.Mostrar($"select * from libros", "libros").Tables[0];
            DtgLibros.AutoResizeColumns();
            LimpiarTxt();
            TxtNombre.Focus();
        }

        void LimpiarTxt()
        {
            TxtNombre.Clear();
            TxtEditorial.Clear();
        }

        private void BtnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Frm_Libros_Load(object sender, EventArgs e)
        {
            ActualizarDtg();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            string resultado = manejador.Insertar(new Libros(0, TxtNombre.Text, TxtEditorial.Text));
            ActualizarDtg();
        }

        private void DtgLibros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            libros.IdLibro = int.Parse(DtgLibros.Rows[e.RowIndex].Cells[0].Value.ToString());
            libros.NombreLibro = DtgLibros.Rows[e.RowIndex].Cells[1].Value.ToString();
            libros.Editorial = DtgLibros.Rows[e.RowIndex].Cells[2].Value.ToString();

            id = libros.IdLibro;
            TxtNombre.Text = libros.NombreLibro;
            TxtEditorial.Text = libros.Editorial;
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejador.Actualizar(libros = new Libros(id, TxtNombre.Text, TxtEditorial.Text));
            ActualizarDtg();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar al libro: {libros.NombreLibro}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                manejador.Eliminar(libros);
                ActualizarDtg();
            }
        }
    }
}
